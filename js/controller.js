// render List Users
function RenderListUsers(data) {
    let content = '';
    data.forEach(user => {
        const {id, taiKhoan, hoTen, matKhau, email, loaiND, ngonNgu} = user;
        content += 
        `
            <tr>
                <td>${id}</td>
                <td>${taiKhoan}</td>
                <td>${hoTen}</td>
                <td>${matKhau}</td>
                <td>${email}</td>
                <td>${loaiND}</td>
                <td>${ngonNgu}</td>
                <td>
                <button onclick='deleteUser(${id})' class='btn btn-danger'>Delete</button>
                <button id='aa' onclick='showInfoFromTable(${id})' class='btn btn-warning'>Reset</button>
                </td>
            </tr>
        `
    });
    return $('#tblDanhSachNguoiDung').innerHTML = content;
};

// DELETE USER
function deleteUser(id) {
    turnOnLoading();
    axios({
        url: `${URL}/Manager/${id}`,
        method: 'DELETE'
    }).then(res => {
        turnOffLoading();
        fetAPI()
    }).catch((err = '404') => {
        turnOffLoading();
    });
};

// show info from table
function showInfoFromTable(id) {
    turnOnLoading();
    axios({ 
        url: `${URL}/Manager/${id}`,
        method: 'GET',
    }).then(res => {
        turnOffLoading();
        getInfoFromTable(res.data);
    callModal('RESET USER', false, 2)
    }).catch((err = '404') => {
        turnOffLoading();
    });
};

// pushUser to FormGroup
function pushUser() { 
    let data = testValidateInput();
    if (data) {
        btnAdd();
        axios({
            url: `${URL}/Manager`,
            method: 'POST',
            data: data
        }).then(res => {
            fetAPI();
            window.location.reload();
        }).catch((err = '404') => { 
           turnOffLoading();
        });
    } 
}